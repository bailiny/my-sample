package com.example.mysample;

import cn.hutool.core.util.RandomUtil;
import com.example.mysample.bloom.BloomFilterUtil;
import com.example.mysample.bloom.RedisBloomFilter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;

@SpringBootTest
class MySampleApplicationTests {
    @Autowired
    private RedisBloomFilter redisBloomFilter;

    @Test
    void contextLoads() {
        redisBloomFilter.containsElement("bloom", "0");
        long start = System.currentTimeMillis();
        Set<String> sets = new HashSet<>();
        int dupCount = 0;
        int errorDup = 0;
        for (int i = 0; i < 1000000; i++) {
            String randomString = RandomUtil.randomString("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", 5);
            if (redisBloomFilter.containsElement("bloom", randomString)) {
                dupCount++;
                System.out.println("第" + i + "次发生重复...");
                if (!sets.contains(randomString)) {
                    errorDup++;
                    System.out.println("属于误判...");
                }
            } else {
                redisBloomFilter.addElement("bloom",randomString);
                sets.add(randomString);
            }
        }
        System.out.println("耗时：" + (System.currentTimeMillis() - start));
        System.out.println("生成：" + sets.size());
        System.out.println("重复：" + dupCount);
        System.out.println("误判：" + errorDup);
    }

}
