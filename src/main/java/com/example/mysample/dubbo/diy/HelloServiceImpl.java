package com.example.mysample.dubbo.diy;

/**
 * @author YangXu
 * @date 2022/10/2
 * @description:
 */
public class HelloServiceImpl implements HelloService {
    @Override
    public String hello(String msg) {
        System.out.println("收到客户端消息: " + msg);
        return "收到服务端信息： " + msg;
    }

}
