package com.example.mysample.dubbo.diy;

public interface HelloService {
    String hello(String ping);
}
