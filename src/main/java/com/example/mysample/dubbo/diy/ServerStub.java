package com.example.mysample.dubbo.diy;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author YangXu
 * @date 2022/10/2
 * @description: 处理请求数据
 */
public class ServerStub extends ChannelInboundHandlerAdapter {
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        String message = msg.toString();
        //人造协议处理
        if (message.split("&")[0].equals("dubbo")) {
            String result = new HelloServiceImpl().hello(message.split("&")[1]);
            ctx.writeAndFlush(result);
        } else {
            ctx.writeAndFlush("不支持该协议！");
        }
    }

}
