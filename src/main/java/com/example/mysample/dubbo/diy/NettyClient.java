package com.example.mysample.dubbo.diy;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author YangXu
 * @date 2022/10/2
 * @description:
 */
public class NettyClient {
    private static ExecutorService executor = Executors
            .newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    private static ClientStub client;
    //自定义协议头
    private static String protocal = "dubbo";

    public static void main(String[] args) throws InterruptedException {
        // 创建一个代理对象
        HelloService service = (HelloService) getBean(HelloService.class, protocal);
        while (true) {
            System.out.println(service.hello("你好 dubbo ~ "));
            Thread.sleep(2000);
        }
    }

    /**
     * 创建一个代理对象
     */
    public static Object getBean(final Class<?> serviceClass, String protocal) {
        return Proxy.newProxyInstance(
                Thread.currentThread().getContextClassLoader(),//类加载器，写活，不要写成目标接口类加载器
                new Class<?>[]{serviceClass},
                new InvocationHandler() {
                    //proxy：代理对象本身，method: 代理对象方法，args: 将来传递的参数-->‘协议头’ 传递进来的信息
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        if (client == null) {
                            initClient();
                        }
                        // 设置参数值，包括协议，具体可以参考zookeeper节点值方式
                        client.setPara(protocal + "&" + args[0].toString());
                        // submit(client)是一个Callable<T>对象---ClientStub
                        // get()拿到ClientStub中实现的call方法的return的结果
                        return executor.submit(client).get();
                    }
                });
    }

    /**
     * 初始化客户端
     */
    private static void initClient() {
        client = new ClientStub();
        /**
         * 内部线程池，内部维护了一组线程，每个线程负责处理多个Channel上的事件，
         *  而一个Channel只对应于一个线程，这样可以回避多线程下的数据同步问题。
         */
        EventLoopGroup group = new NioEventLoopGroup();
        //服务消费方Bootstrap
        Bootstrap b = new Bootstrap();
        b.group(group)//设置线程组
                .channel(NioSocketChannel.class)//使用NIO通道
                .handler(new ChannelInitializer<SocketChannel>() {//处理通道请求事件
                    public void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline p = ch.pipeline();
                        p.addLast(new StringDecoder());//解码器
                        p.addLast(new StringEncoder());//编码器
                        p.addLast(client);//业务处理器消费方Stub
                    }
                });
        try {
            //建立长连接
            b.connect("127.0.0.1", 7002).await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
