package com.example.mysample.dubbo.diy;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.concurrent.Callable;

/**
 * @author YangXu
 * @date 2022/10/2
 * @description: 客户端业务处理
 */
public class ClientStub extends ChannelInboundHandlerAdapter implements Callable {
    private ChannelHandlerContext context;
    private String result;//客户端调用后,返回的结果
    private String para;//客户端调用方法时传递进来的参数

    /**
     * 与服务器的连接已经建立之后将被调用，ctx传递给context,做成属性
     *
     * @param ctx
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        //其它方法里面 使用到当前Handler里面的上下文,因此需要保存下上下文当成属性
        context = ctx;
    }

    /**
     * 收到服务端数据，唤醒等待线程，与Callable实现方法call同步，需要加synchronized
     */
    @Override
    public synchronized void channelRead(ChannelHandlerContext ctx, Object msg) {
        result = msg.toString();
        notify();
    }

    /**
     * 数据发出，等待channelRead处理后被唤醒，与
     * ChannelInboundHandlerAdapter实现方法channelRead同步，需要加synchronized
     */
    @Override
    public synchronized Object call() throws InterruptedException {
        context.writeAndFlush(para);
        //如果不加该阻塞，服务器返回数据为空或者多个返回会一起设置到result
        wait();
        return result;
    }

    public void setPara(String para) {
        this.para = para;
    }

}
