package com.example.mysample.dubbo.spi;

import org.apache.dubbo.common.URL;

/**
 * @author YangXu
 * @date 2022/10/1
 * @description:
 */
public class BMWCar implements Car {
    @Override
    public void run(URL url) {
        System.out.println("我是宝马");
    }
}
