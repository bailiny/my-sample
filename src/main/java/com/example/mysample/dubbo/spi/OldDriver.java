package com.example.mysample.dubbo.spi;

import org.apache.dubbo.common.URL;

/**
 * @author YangXu
 * @date 2022/10/1
 * @description:
 */
public class OldDriver implements Driver {
    private Car car;

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public void driverCar(URL url) {
        System.out.println("哥哥考了驾照十年了......");
        car.run(url);
    }
}
