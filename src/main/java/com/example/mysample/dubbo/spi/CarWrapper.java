package com.example.mysample.dubbo.spi;

import org.apache.dubbo.common.URL;

/**
 * @author YangXu
 * @date 2022/10/1
 * @description: dubbo spi实现aop
 */
public class CarWrapper implements Car {
    private Car car;

    public CarWrapper(Car car) {
        this.car = car;
    }

    @Override
    public void run(URL url) {
        System.out.println("哥哥存钱喽.........");
        car.run(url);
        System.out.println("哥哥买车喽.........");
    }

}
