package com.example.mysample.dubbo.spi;

import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.extension.SPI;

@SPI("old")
public interface Driver {
    void driverCar(URL url);
}
