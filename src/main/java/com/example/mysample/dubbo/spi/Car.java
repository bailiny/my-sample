package com.example.mysample.dubbo.spi;

import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.extension.Adaptive;
import org.apache.dubbo.common.extension.SPI;

/**
 * 汽车接口类
 * @SPI 可以设置默认值，如果使用默认值那么在按需加载的地方传参为"true"即可
 */
@SPI
public interface Car {
    @Adaptive(value = "carType")
    void run(URL url);
}
