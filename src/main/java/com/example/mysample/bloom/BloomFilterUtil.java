package com.example.mysample.bloom;

import cn.hutool.core.util.RandomUtil;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

import java.nio.charset.Charset;

/**
 * @author YangXu
 * @date 2022/10/13
 * @description:
 */
public class BloomFilterUtil {
    private BloomFilterUtil() {
    }


    public static class BloomFilterUtilInstance {
        private static final BloomFilterUtil INSTANCE = new BloomFilterUtil();
    }

    public static BloomFilterUtil getInstance() {
        return BloomFilterUtilInstance.INSTANCE;
    }

    /**
     * 预估数据量
     */
    private static final int INSERTIONS = 10000000;

    /**
     * 判重错误率
     */
    private static final double FPP = 0.00001;

    private BloomFilter<String> bloomFilter = BloomFilter.create(Funnels.stringFunnel(Charset.defaultCharset()), INSERTIONS, FPP);

    public void addElement(String value) {
        bloomFilter.put(value);
    }

    public boolean containsElement(String value) {
        return bloomFilter.mightContain(value);
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        BloomFilterUtil instance = BloomFilterUtil.getInstance();
        int dupCount = 0;
        for (int i = 0; i < 10000000; i++) {
            String randomString = RandomUtil.randomString("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", 8);
            if (instance.containsElement(randomString)) {
                dupCount++;
                System.out.println("第" + i + "次发生重复...");
            } else {
                instance.addElement(randomString);
            }
        }
        System.out.println("执行完毕，耗时" + (System.currentTimeMillis() - start) + "ms，重复次数：" + dupCount);
    }
}
