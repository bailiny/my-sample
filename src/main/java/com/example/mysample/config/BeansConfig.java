package com.example.mysample.config;

import com.example.mysample.bloom.BloomFilterHelper;
import com.google.common.base.Charsets;
import com.google.common.hash.Funnel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author YangXu
 * @date 2022/10/17
 * @description:
 */
@Component
public class BeansConfig {
    protected Logger logger = LoggerFactory.getLogger(getClass());
    @Bean
    public BloomFilterHelper<String> bloomFilterHelper() {
        logger.info("加载布隆过滤器工具类");
        return new BloomFilterHelper<>((Funnel<String>) (from, into) -> into.putString(from, Charsets.UTF_8).putString(from, Charsets.UTF_8), 1000000, 0.001);
    }
}
